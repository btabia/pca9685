// I2C scanner

#include "mbed.h"


Serial pc(USBTX, USBRX);
I2C i2c(D14, D15);


void setup(){
  i2c.frequency(400000);
  pc.baud(115200);
  pc.printf("I2C SCANNER BEGIN\n");
}

void testSpecific(uint8_t address){
  uint8_t error;
  i2c.start();
  error = i2c.write(address << 1);
  if(error == 1){
    pc.printf(" Test succeed\n");
  }
  else{
    pc.printf(" No detection\n");
  }
  i2c.stop();
  wait(1);
}

void loop(void){
  uint8_t error, address;
  uint8_t nb_device = 0;
  for(int i = 0; i < 127; ++i){
    address = i;
    i2c.start();
    wait(2);
    error = i2c.write(address << 1);
    if(error == 1){
      pc.printf("iteration %d :system %d is responding, result is %d\n",address,address, error);
      nb_device++;
    }
    else if(error == 2){
      pc.printf("iteration %d :system is not responding, result is %d\n",address, error);
    }
    else{
      pc.printf("iteration %d:system is not responding, result is %d\n",address, error);
    }
    wait(1);
    i2c.stop();
    if(i == 127){
      pc.printf(" END OF TEST, there is %d devices \n", nb_device);
    }
  }
}
int main (void){
  setup();
  while(1){
    //loop();
    testSpecific(0x40);
  }
}
