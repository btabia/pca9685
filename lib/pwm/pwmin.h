#ifndef PWM_IN_H
#define PWM_IN_H

#include "mbed.h"

class PwmIn{
public:
  PwmIn(PinName  _pin);
  float period();
  float pulseWidth();
  float dutycycle();

protected:
  void rise();
  void fall();

  InterruptIn _p;
  Timer _t;
  float _pulsewidth;
  float _period;
};

#endif
