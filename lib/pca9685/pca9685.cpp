#include "pca9685.h"

PCA9685::PCA9685(I2C& _i2c):
  i2c(_i2c){}

void PCA9685::begin(void){
  i2c.start();
  reset();
  // Set a default frequency (50Hz)
  setPWMFreq(50);
}

void PCA9685::reset(void){
  writeByte(PCA9685_ADDR, PCA9685_MODE1, 0x80);
  wait_ms(10);
}

void PCA9685::setPWMFreq(float freq){
  freq *= 0.9;  // Correct for overshoot in the frequency setting (see issue #11).
  float prescaleval = 25000000;
  prescaleval /= 4096;
  prescaleval /= freq;
  prescaleval -= 1;
  uint8_t prescale = floor(prescaleval + 0.5);
  uint8_t oldmode = readByte(PCA9685_ADDR, PCA9685_MODE1);
  uint8_t newmode = (oldmode&0x7F) | 0x10; // sleep
  writeByte(PCA9685_ADDR,PCA9685_MODE1, newmode); // go to sleep`
  writeByte(PCA9685_ADDR,PCA9685_PRESCALE, prescale); // set the prescaler
  writeByte(PCA9685_ADDR,PCA9685_MODE1, oldmode);
  wait_ms(5);
  writeByte(PCA9685_ADDR,PCA9685_MODE1, oldmode | 0xa0); //  This sets the MODE1 register to turn on auto increment.
}

int PCA9685::setPWM(uint8_t num, uint16_t on, uint16_t off){
  uint8_t channel = 0x00;
  int ret = 3;
  i2c.start();
  ret = i2c.write(PCA9685_ADDR);
  ret = i2c.write(0x00 << 1);
  ret = i2c.write(on);
  ret = i2c.write(on >> 8);
  ret = i2c.write(off);
  ret = i2c.write(off >> 8);
  i2c.stop();
  return ret;
  //writeBytes(PCA9685_ADDR, channel, 4, pwm);
}

float PCA9685::getPWM(uint8_t num){
  uint8_t data[4];
  uint8_t channel = LED0_ON_L+4*num;
  readBytes(PCA9685_ADDR, channel, 4, data);
  uint16_t on = (int16_t)((int16_t)(data[0] << 8) | (int16_t)(data[1]));
  uint16_t off = (int16_t)((int16_t)(data[2] << 8) | (int16_t)(data[3]));
  float pwm = (off - on) / 4096;
  return pwm;
}

/**** I2C method for communication */

void PCA9685::writeByte(uint8_t addr, uint8_t subaddr, uint8_t data){
  char _data[2];
  _data[0] = subaddr;
  _data[1] = data;
  i2c.write(addr<<1, _data, 2);
}

void PCA9685::writeBytes(uint8_t addr, uint8_t subaddr, uint8_t count, uint8_t *pData){
  char *_data = new char[count + 1];
  _data[0] = subaddr;
  for(int i = 1; i < count + 1; ++i){
    _data[i] = pData[i];
  }
  i2c.write(addr<<1, _data, count,0);
  delete _data;
}

char PCA9685::readByte(uint8_t addr, uint8_t subaddr){
  char data[1];
  char data_write[1];
  data_write[0] = char(subaddr);
  i2c.write(addr<<1, &data_write[0], 1);
  i2c.read(addr<<1, &data[0], 1);
  return data[0];
}

void PCA9685::readBytes(uint8_t addr, uint8_t subaddr, uint8_t count, uint8_t *pData){
  char *data = new char[count];
  char write_data[1];
  write_data[0] = subaddr;
  i2c.write(addr<<1, write_data, 1, 1);
  i2c.read(addr<<1, data, count, 0);
  for(int i = 0; i < count; ++i){
    pData[i] = data[i];
  }
  delete data;
}
