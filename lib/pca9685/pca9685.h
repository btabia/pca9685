#ifndef PCA9685_H
#define PCA9685_H

// This driver is made for the PCA9685
// A 16 channel and 12 bit led driver
#include "mbed.h"

#define PCA9685_ADDR    0x40
#define PCA9685_SUBADR1 0x02
#define PCA9685_SUBADR2 0x03
#define PCA9685_SUBADR3 0x04

#define PCA9685_MODE1 0x00
#define PCA9685_MODE2 0x01
#define PCA9685_PRESCALE 0xFE

#define LED0_ON_L 0x06
#define LED0_ON_H 0x07
#define LED0_OFF_L 0x08
#define LED0_OFF_H 0x09

#define ALLLED_ON_L 0xFA
#define ALLLED_ON_H 0xFB
#define ALLLED_OFF_L 0xFC
#define ALLLED_OFF_H 0xFD
#define OK 1

class PCA9685{
public:
  PCA9685(I2C& _i2c);
  void begin(void);
  void reset(void);
  void setPWMFreq(float freq);
  int setPWM(uint8_t num, uint16_t on, uint16_t off);
  void setPin(uint8_t num, uint16_t val, bool invert = false);
  float getPWM(uint8_t num);
  // void init(void);
  // void reset(void);
  // int setPWMFreq(float freq);
  // int setPWM(uint8_t pin, float pwm_percentage);


private:
  I2C& i2c;
  void writeByte(uint8_t addr, uint8_t subaddr, uint8_t data);
  void writeBytes(uint8_t addr, uint8_t subaddr, uint8_t count, uint8_t *pData);
  char readByte(uint8_t addr, uint8_t subaddr);
  void readBytes(uint8_t addr, uint8_t subaddr, uint8_t count, uint8_t *pData);
};

#endif /* PWM PCA9685_H */
