// Test code of the PCA9685 driver for the control of servomotor actuator
// Author : Bechir TABIA
// Date : 15/11/2018
#include <mbed.h>
#include <pca9685.h>
#include "../lib/pwm/pwmin.h"

I2C con(D14,D15);
PCA9685 SC(con);
Serial pc(USBTX,USBRX);
//PwmIn pwm3(D9);
//PwmOut pout(D6);

void init(void){
  pc.baud(115200);
  con.frequency(400000);
  SC.begin();
  // pout.period_ms(20);
  // pout.write(0.20);
  pc.printf("PCA9285 TEST BEGIN\n");
  pc.printf("I2C TEST ADDRESS 0X40\n");
  uint8_t error = con.write(0x40 << 1);
  if(error == 1){
    pc.printf("PCA9285 respond\n");
  }
  else{
    pc.printf("ERROR: PCA9285 don't respond\n");
  }
}

void testPwmOut(void){
  int test = 0;
  uint8_t pin = 0;
  float pwm = 0.8;
  int t = SC.setPWM(pin, 0, 3000);
  // pout.write(pwm);
  // pc.printf("test i2c %d pwm_ref: %f pwm_mes %f\n", t,pwm, pwm3.dutycycle());
  // read pwm
  //test connection
  test = con.write(64 << 1);
  if(test == 1){
    pc.printf("connection to PCA ok \r");
  }
  else{
    pc.printf("connection to PCA not ok \r");
  }
  wait_ms(100);
}

char readByte(uint8_t addr, uint8_t subaddr){
  char data[1];
  char data_write[1];
  data_write[0] = char(subaddr);
  con.write(addr<<1, data_write, 1);
  con.read(addr<<1, data, 1);
  return data[0];
}

void readModeRegister(void){
  char data[2];
  data[0] = readByte(PCA9685_ADDR, PCA9685_MODE2);
  data[1] = readByte(PCA9685_ADDR, PCA9685_MODE1);
  pc.printf("Mode1 : %d / Mode2 : %d \r", data[0], data[1]);
}

int main (){
  init();
  while(1){
    readModeRegister();
  }
}
